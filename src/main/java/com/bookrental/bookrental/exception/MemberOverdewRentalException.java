package com.bookrental.bookrental.exception;

public class MemberOverdewRentalException extends AppException {

    public MemberOverdewRentalException(String message) {
        super(message);
    }
}
