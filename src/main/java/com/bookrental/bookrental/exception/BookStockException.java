package com.bookrental.bookrental.exception;

public class BookStockException extends AppException{
    public BookStockException(String message) {
        super(message);
    }
}
