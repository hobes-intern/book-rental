package com.bookrental.bookrental.exception;

public class CategoryAlreadyExistsException extends AppException {
    public CategoryAlreadyExistsException(String message) {
        super(message);
    }
}
