package com.bookrental.bookrental.exception;

public class MemberException extends AppException{
    public MemberException(String message) {
        super(message);
    }
}
