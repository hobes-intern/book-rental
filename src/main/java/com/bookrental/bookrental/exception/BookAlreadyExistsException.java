package com.bookrental.bookrental.exception;

public class BookAlreadyExistsException extends AppException {
    public BookAlreadyExistsException(String message) {
        super(message);
    }
}
